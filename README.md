# Zápočtový projekt šachy na mobil

## Co je projekt zač

Tento projekt jsou šachy, určené na hraní na mobilu. Módy, které je schopen člověk hrát jsou:

- Hot seat, dva hráči na jednom počítači.

## V čem jsou tvořeny?

Šachy jsou tvořeny v Javě.

## Backend a frontend

Jeden z cílů co jsem si dal, je rozdělit backend a frontend co nejvíce, abych mohl backend využít i na jiné platformy.
