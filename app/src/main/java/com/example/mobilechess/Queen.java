package com.example.mobilechess;

public class Queen {

    /**
     * Calculates possible moves for the Queen at the specified position.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the Bishop.
     * @param j            The current column index of the Bishop.
     * @return {@code true} if there are possible moves for the Bishop, {@code false} otherwise.
     */
    public static boolean setMoves(boolean justChecking, int i, int j)
    {
        boolean wasThereMove = false;
        if (Bishop.setMoves(justChecking, i, j))
        {
            wasThereMove = true;
        }
        if (Rook.setMoves(justChecking, i, j))
        {
            wasThereMove = true;
        }
        return wasThereMove;
    }
}