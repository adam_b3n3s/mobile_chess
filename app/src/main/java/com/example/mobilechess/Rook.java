package com.example.mobilechess;

public class Rook {

    public static boolean ZZrookMove = false;
    public static boolean ZSrookMove = false;
    public static boolean SZrookMove = false;
    public static boolean SSrookMove = false;

    /**
     * Calculates possible moves for the Rook at the specified position.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the Bishop.
     * @param j            The current column index of the Bishop.
     * @return {@code true} if there are possible moves for the Bishop, {@code false} otherwise.
     */
    public static boolean setMoves(boolean justChecking, int i, int j)
    {
        boolean wasThereMove = false;
        String smallerGrid[][] = Backend.convertToFigures(Backend.grid);
        for (int k = i + 1; k < 8; k++)
        {
            if (Backend.grid[k][j][1].equals("") || Backend.grid[k][j][1].equals("EP"))
            {
                if (Backend.testForPossibleMove(justChecking, i, j, k, j, Backend.currentPlayer, smallerGrid, "R"))
                {
                    wasThereMove = true;
                }
            }
            else
            {
                if ((Backend.grid[k][j][1].endsWith("B")
                 && Backend.currentPlayer == Backend.Players.WHITE)
                 || (Backend.grid[k][j][1].endsWith("W")
                 && Backend.currentPlayer == Backend.Players.BLACK))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, k, j, Backend.currentPlayer, smallerGrid, "R"))
                    {
                        wasThereMove = true;
                    }
                }
                break;
            }
        }
        for (int k = i - 1; k >= 0; k--)
        {
            if (Backend.grid[k][j][1].equals("") || Backend.grid[k][j][1].equals("EP"))
            {
                if (Backend.testForPossibleMove(justChecking, i, j, k, j, Backend.currentPlayer, smallerGrid, "R"))
                {
                    wasThereMove = true;
                }
            }
            else
            {
                if ((Backend.grid[k][j][1].endsWith("B")
                 && Backend.currentPlayer == Backend.Players.WHITE)
                 || (Backend.grid[k][j][1].endsWith("W")
                 && Backend.currentPlayer == Backend.Players.BLACK))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, k, j, Backend.currentPlayer, smallerGrid, "R"))
                    {
                        wasThereMove = true;
                    }
                }
                break;
            }
        }
        for (int k = j + 1; k < 8; k++)
        {
            if (Backend.grid[i][k][1].equals("") || Backend.grid[i][k][1].equals("EP"))
            {
                if (Backend.testForPossibleMove(justChecking, i, j, i, k, Backend.currentPlayer, smallerGrid, "R"))
                {
                    wasThereMove = true;
                }
            }
            else
            {
                if ((Backend.grid[i][k][1].endsWith("B")
                 && Backend.currentPlayer == Backend.Players.WHITE)
                 || (Backend.grid[i][k][1].endsWith("W")
                 && Backend.currentPlayer == Backend.Players.BLACK))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i, k, Backend.currentPlayer, smallerGrid, "R"))
                    {
                        wasThereMove = true;
                    }
                }
                break;
            }
        }
        for (int k = j - 1; k >= 0; k--)
        {
            if (Backend.grid[i][k][1].equals("") || Backend.grid[i][k][1].equals("EP"))
            {
                if (Backend.testForPossibleMove(justChecking, i, j, i, k, Backend.currentPlayer, smallerGrid, "R"))
                {
                    wasThereMove = true;
                }
            }
            else
            {
                if ((Backend.grid[i][k][1].endsWith("B")
                 && Backend.currentPlayer == Backend.Players.WHITE)
                 || (Backend.grid[i][k][1].endsWith("W")
                 && Backend.currentPlayer == Backend.Players.BLACK))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i, k, Backend.currentPlayer, smallerGrid, "R"))
                    {
                        wasThereMove = true;
                    }
                }
                break;
            }
        }
        return wasThereMove;
    }
}
