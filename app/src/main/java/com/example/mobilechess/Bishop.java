package com.example.mobilechess;

public class Bishop {

    /**
     * Calculates possible moves for the Bishop at the specified position.
     *
     * @param justChecking Indicates whether to only check the move without making any changes.
     * @param i            The current row index of the Bishop.
     * @param j            The current column index of the Bishop.
     * @return {@code true} if there are possible moves for the Bishop, {@code false} otherwise.
     */
    public static boolean setMoves(boolean justChecking, int i, int j)
    {
        boolean wasThereMove = false;
        String smallerGrid[][] = Backend.convertToFigures(Backend.grid);
        for (int k = 1; k < 8; k++)
        {
            if (i + k < 8 && j + k < 8)
            {
                if (Backend.grid[i + k][j + k][1].equals("") || Backend.grid[i + k][j + k][1].equals("EP"))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i + k, j + k, Backend.currentPlayer, smallerGrid, "B"))
                    {
                        wasThereMove = true;
                    }
                }
                else
                {
                    if ((Backend.grid[i + k][j + k][1].endsWith("B")
                     && Backend.currentPlayer == Backend.Players.WHITE)
                     || (Backend.grid[i + k][j + k][1].endsWith("W")
                     && Backend.currentPlayer == Backend.Players.BLACK))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + k, j + k, Backend.currentPlayer, smallerGrid, "B"))
                        {
                            wasThereMove = true;
                        }
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }
        for (int k = 1; k < 8; k++)
        {
            if (i + k < 8 && j - k >= 0)
            {
                if (Backend.grid[i + k][j - k][1].equals("") || Backend.grid[i + k][j - k][1].equals("EP"))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i + k, j - k, Backend.currentPlayer, smallerGrid, "B"))
                    {
                        wasThereMove = true;
                    }
                }
                else
                {
                    if ((Backend.grid[i + k][j - k][1].endsWith("B")
                     && Backend.currentPlayer == Backend.Players.WHITE)
                     || (Backend.grid[i + k][j - k][1].endsWith("W")
                     && Backend.currentPlayer == Backend.Players.BLACK))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i + k, j - k, Backend.currentPlayer, smallerGrid, "B"))
                        {
                            wasThereMove = true;
                        }
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }
        for (int k = 1; k < 8; k++)
        {
            if (i - k >= 0 && j - k >= 0)
            {
                if (Backend.grid[i - k][j - k][1].equals("") || Backend.grid[i - k][j - k][1].equals("EP"))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i - k, j - k, Backend.currentPlayer, smallerGrid, "B"))
                    {
                        wasThereMove = true;
                    }
                }
                else
                {
                    if ((Backend.grid[i - k][j - k][1].endsWith("B")
                    && Backend.currentPlayer == Backend.Players.WHITE)
                    || (Backend.grid[i - k][j - k][1].endsWith("W")
                    && Backend.currentPlayer == Backend.Players.BLACK))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - k, j - k, Backend.currentPlayer, smallerGrid, "B"))
                        {
                            wasThereMove = true;
                        }
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }
        for (int k = 1; k < 8; k++)
        {
            if (i - k >= 0 && j + k < 8)
            {
                if (Backend.grid[i - k][j + k][1].equals("") || Backend.grid[i - k][j + k][1].equals("EP"))
                {
                    if (Backend.testForPossibleMove(justChecking, i, j, i - k, j + k, Backend.currentPlayer, smallerGrid, "B"))
                    {
                        wasThereMove = true;
                    }
                }
                else
                {
                    if ((Backend.grid[i - k][j + k][1].endsWith("B")
                    && Backend.currentPlayer == Backend.Players.WHITE)
                    || (Backend.grid[i - k][j + k][1].endsWith("W")
                    && Backend.currentPlayer == Backend.Players.BLACK))
                    {
                        if (Backend.testForPossibleMove(justChecking, i, j, i - k, j + k, Backend.currentPlayer, smallerGrid, "B"))
                        {
                            wasThereMove = true;
                        }
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }
        return wasThereMove;
    }
}
